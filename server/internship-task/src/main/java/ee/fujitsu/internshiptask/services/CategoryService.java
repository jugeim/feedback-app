package ee.fujitsu.internshiptask.services;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.services.dto.CategoryDto;
import ee.fujitsu.internshiptask.services.exceptions.CategoryNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service to hide CategoryRepository related logic from controller
 * Will take and return DTOs and internally work with Entity model
 */
@Service("CategoryService")
public class CategoryService {

    private final CategoryRepository categories;

    @Autowired
    public CategoryService(CategoryRepository categories) {
        this.categories = categories;
    }

    /**
     * Method to get all category entities stored in DB
     * @return list of categories converted into DTOs
     */
    public List<CategoryDto> getAll() {
        return categories.findAll().stream().map(CategoryDto::new).collect(Collectors.toList());
    }

    /**
     * Method to get category entity with provided id, if there is one in DB.
     * Throws <c>CategoryNotFoundException</c> in case entity with provided id was not found
     * Throws <c>InvalidIdentifierException</c> in case was provided invalid (not of UUID format) id
     * @param id String representation of id (UUID) of category
     * @return found category DTO
     */
    public CategoryDto getOne(String id) {
        try {
            return categories.findById(UUID.fromString(id))
                    .map(CategoryDto::new)
                    .orElseThrow(() -> new CategoryNotFoundException(id));
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }
    }

    /**
     * Saves input data (provided in form of CategoryDto) into Category table and returns Category DTO of saved category
     * @param category category data in form of DTO
     * @return CategoryDto of saved category
     */
    public CategoryDto save(CategoryDto category) {
        Category toSave = new Category(category.getName());

        if (category.getCatalogCategoryId() != null) {
            Category catalogCategory = categories.findById(UUID.fromString(category.getCatalogCategoryId()))
                                                 .orElse(null);
            toSave.setCatalogCategory(catalogCategory);
        }
        return new CategoryDto(categories.save(toSave));
    }

    /**
     * Finds category from DB with provided id and sets its values to values provided as CategoryDto argument
     * @param category CategoryDto with update data
     * @param id reference to category in DB which will be updated
     * @return CategoryDto of updated category
     */
    public CategoryDto update(CategoryDto category, String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }
        Category catalogCategory = getCategoryByIdString(category.getCatalogCategoryId());

        Category updatedCategory = categories.findById(uuid)
                .map(cat -> {
                    cat.setName(category.getName());
                    cat.setCatalogCategory(catalogCategory);
                    return categories.save(cat);
                })
                .orElseGet(() -> {
                    Category newCategory = new Category(category.getName(), catalogCategory);
                    newCategory.setId(uuid);
                    return categories.save(newCategory);
                });

        return new CategoryDto(updatedCategory);
    }

    /**
     * Delete category with PK given as argument
     * @param id category PK
     */
    public void delete(String id) {
        try {
            categories.deleteById(UUID.fromString(id));
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }
    }

    private Category getCategoryByIdString(String categoryId) {
        if (categoryId == null) return null;

        UUID catalogCategoryId = UUID.fromString(categoryId);

        return categories.findById(catalogCategoryId).orElse(null);
    }
}
