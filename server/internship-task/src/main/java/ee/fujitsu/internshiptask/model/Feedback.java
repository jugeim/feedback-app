package ee.fujitsu.internshiptask.model;

import lombok.Data;

import javax.persistence.*;
import java.util.*;

/**
 * Entity model for table 'feedback'
 */
@Entity
@Table(name="feedback")
@Data
public class Feedback {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;
    private String email;

    @Lob
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time = new Date();

    /**
     * Will autogenerate middle table 'feedback_category'(with composite PK, consisting of both FKs)
     * between 'feedback' and 'category' tables
     */
    @ManyToMany(fetch = FetchType.EAGER) // automatically load categories when feedback requested
    @JoinTable(
            name = "feedback_category",
            joinColumns = @JoinColumn(name = "feedback_id"),
            inverseJoinColumns = @JoinColumn(name="category_id")
    )
    private List<Category> categories = new ArrayList<>();

    public Feedback() {}

    public Feedback(String name, String email, String text) {
        this.name = name;
        this.email = email;
        this.text = text;
    }

    public Feedback(String name, String email, String text, Category... categories) {
        this.name = name;
        this.email = email;
        this.text = text;
        this.categories.addAll(Arrays.asList(categories));
    }

}
