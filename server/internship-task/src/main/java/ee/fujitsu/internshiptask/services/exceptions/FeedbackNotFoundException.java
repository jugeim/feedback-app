package ee.fujitsu.internshiptask.services.exceptions;

public class FeedbackNotFoundException extends EntityNotFoundException {

    private static final String entityName = "Feedback";

    public FeedbackNotFoundException(String id) {
        super(entityName, id);
    }
}
