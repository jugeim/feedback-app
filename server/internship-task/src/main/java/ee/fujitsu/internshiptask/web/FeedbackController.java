package ee.fujitsu.internshiptask.web;


import ee.fujitsu.internshiptask.services.FeedbackService;
import ee.fujitsu.internshiptask.services.dto.FeedbackDto;
import ee.fujitsu.internshiptask.services.exceptions.FeedbackNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api/feedbacks")
public class FeedbackController {

    private final FeedbackService service;

    FeedbackController(FeedbackService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<FeedbackDto>> all() {

        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("")
    public ResponseEntity<FeedbackDto> newFeedback(@RequestBody FeedbackDto feedbackFromRequest, UriComponentsBuilder ucb) {

        var saved = service.save(feedbackFromRequest);

        return ResponseEntity
                .created(ucb.path("/api/feedbacks/").path(saved.getId()).build().toUri())
                .body(saved);
    }

    // Single item
    @GetMapping("/{id}")
    public ResponseEntity<FeedbackDto> one(@PathVariable String id) {

        FeedbackDto feedback = service.getOne(id);

        return ResponseEntity.ok(feedback);
    }

    @ResponseBody
    @ExceptionHandler(FeedbackNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String feedbackNotFoundHandler(FeedbackNotFoundException e) {
        return e.getMessage();
    }

    @PutMapping("/{id}")
    public ResponseEntity<FeedbackDto> replaceFeedback(@RequestBody FeedbackDto feedbackFromRequest, @PathVariable String id,
                                             UriComponentsBuilder ucb) {

        FeedbackDto updated = service.update(feedbackFromRequest, id);

        return ResponseEntity
                .created(ucb.path("/api/feedbacks/").path(updated.getId()).build().toUri())
                .body(updated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<FeedbackDto> deleteFeedback(@PathVariable String id) {

        service.delete(id);

        return ResponseEntity.noContent().build();
    }

    @ResponseBody
    @ExceptionHandler(InvalidIdentifierException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String invalidIdentifierHandler(InvalidIdentifierException e) {
        return e.getMessage();
    }

}
