package ee.fujitsu.internshiptask.services.dto;

import ee.fujitsu.internshiptask.model.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for clients (will be exposed in controllers)
 */
@Data
@NoArgsConstructor
public class CategoryDto {

    private String id;
    private String name;
    private String catalogCategoryId;

    /**
     * Constructor to use instead of using mapper
     * @param category Category from DB that will be mapped into DTO
     */
    public CategoryDto(Category category) {
        if (category == null) return;

        id = category.getId() != null ? category.getId().toString() : null;
        name = category.getName();

        if (category.getCatalogCategory() != null && category.getCatalogCategory().getId() != null) {
            catalogCategoryId = category.getCatalogCategory().getId().toString();
        }
    }

}