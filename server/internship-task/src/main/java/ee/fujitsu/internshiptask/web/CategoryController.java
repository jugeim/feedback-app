package ee.fujitsu.internshiptask.web;

import ee.fujitsu.internshiptask.services.CategoryService;
import ee.fujitsu.internshiptask.services.dto.CategoryDto;
import ee.fujitsu.internshiptask.services.exceptions.CategoryNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api/categories")
public class CategoryController {
    private final CategoryService service;

    CategoryController(CategoryService service) {
        this.service = service;
    }


    @GetMapping
    public ResponseEntity<List<CategoryDto>> all() {

        return ResponseEntity.ok(service.getAll());

    }

    @PostMapping("")
    public ResponseEntity<CategoryDto> newCategory(@RequestBody CategoryDto categoryFromRequest, UriComponentsBuilder ucb) {

        var saved = service.save(categoryFromRequest);

        return ResponseEntity
                .created(ucb.path("/api/categories/").path(saved.getId()).build().toUri())
                .body(saved);
    }

    // Single item

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDto> one(@PathVariable String id) {

        CategoryDto category = service.getOne(id);

        return ResponseEntity.ok(category);
    }

    @ResponseBody
    @ExceptionHandler(CategoryNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String categoryNotFoundHandler(CategoryNotFoundException e) {
        return e.getMessage();
    }

    @PutMapping("/{id}")
    public ResponseEntity<CategoryDto> replaceCategory(@RequestBody CategoryDto categoryFromRequest, @PathVariable String id,
                                             UriComponentsBuilder ucb) {

        CategoryDto updated = service.update(categoryFromRequest, id);

        return ResponseEntity
                .created(ucb.path("/api/categories/").path(updated.getId()).build().toUri())
                .body(updated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CategoryDto> deleteCategory(@PathVariable String id) {

        service.delete(id);

        return ResponseEntity.noContent().build();
    }

    @ResponseBody
    @ExceptionHandler(InvalidIdentifierException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String invalidIdentifierHandler(InvalidIdentifierException e) {
        return e.getMessage();
    }

}
