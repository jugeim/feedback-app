package ee.fujitsu.internshiptask;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.repositories.FeedbackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@TestConfiguration // fixes problems with CategoryServiceTest and CategoryControllerTest
public class DatabaseInit {

    private static final Logger log = LoggerFactory.getLogger(DatabaseInit.class);

    @Bean
    CommandLineRunner initDatabase(FeedbackRepository feedbackRepo, CategoryRepository categoryRepo) {
        Category mainCategory = new Category("Application");

        Category health = new Category("Health", mainCategory);
        Category docManagement = new Category("Document management", mainCategory);

        List<Category> healthSubCategories = createCategoriesUnderCatalog(health,
                "Patients portal", "Doctors portal");

        Category visitsPortal = new Category("Remote visits portal", health);

        List<Category> visitsPortalSubCategories = createCategoriesUnderCatalog(visitsPortal,
                "Registration", "Virtual visit");

        List<Category> docManagementSubCategories = createCategoriesUnderCatalog(docManagement,
                "OpenKM", "Microsoft SharePoint");

        List<Category> toInsert = new ArrayList<>(List.of(mainCategory, health, docManagement, visitsPortal));
        toInsert.addAll(healthSubCategories);
        toInsert.addAll(docManagementSubCategories);
        toInsert.addAll(visitsPortalSubCategories);

        for (Category category : toInsert) log.info("Preloading "+ categoryRepo.save(category));

        Feedback demoFeedback = new Feedback("Otto Lakk", "test@fujitsu.ee", "Feedback text",
                healthSubCategories.get(0), docManagementSubCategories.get(1));

        return args -> {
            log.info("Preloading " + feedbackRepo.save(demoFeedback));
            log.info("Database preloading complete");
        };
    }



    private List<Category> createCategoriesUnderCatalog(Category catalogCategory, String... categoryNames) {
        List<Category> res = new ArrayList<>();
        for (String categoryName : categoryNames) {
            res.add(new Category(categoryName, catalogCategory));
        }
        return res;
    }
}
