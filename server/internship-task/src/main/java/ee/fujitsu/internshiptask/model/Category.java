package ee.fujitsu.internshiptask.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Entity model for table 'category'
 */
@Entity
@Table(name="category")
@Data
public class Category {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    @ToString.Exclude
    @ManyToOne
    private Category catalogCategory;


    public Category() {}

    public Category(String name) {
        this.name = name;
        this.catalogCategory = null;
    }

    public Category(String name, Category catalogCategory) {
        this.name = name;
        this.catalogCategory = catalogCategory;
    }


    @ToString.Exclude
    @ManyToMany(mappedBy = "categories")
    private List<Feedback> feedbacks = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "catalogCategory")
    private List<Category> subCategories = new ArrayList<>();


    @ToString.Include
    public String catalogCategoryStr() {
        if (catalogCategory == null) return "null";
        return String.format("{id: %s, name: %s}", this.catalogCategory.getId(), this.catalogCategory.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Category)) return false;

        Category other = (Category) obj;

        return this.getId() == null && other.getId() == null
                || this.getId() != null && this.getId().equals(other.getId())
                && this.getName() != null && this.getName().equals(other.getName())
                && this.catalogCategoryStr().equals(other.catalogCategoryStr());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, catalogCategoryStr());
    }

}
