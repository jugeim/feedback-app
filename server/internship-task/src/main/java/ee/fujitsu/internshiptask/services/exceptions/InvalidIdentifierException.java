package ee.fujitsu.internshiptask.services.exceptions;

public class InvalidIdentifierException extends RuntimeException {

    public InvalidIdentifierException(String invalidId) {
        super(String.format("Provided id (%s) of invalid format", invalidId));
    }
}
