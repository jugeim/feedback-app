package ee.fujitsu.internshiptask.services;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.repositories.FeedbackRepository;
import ee.fujitsu.internshiptask.services.dto.FeedbackDto;
import ee.fujitsu.internshiptask.services.exceptions.FeedbackNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service to hide FeedbackRepository (and CategoryRepository) related logic from controller
 * Will take and return DTOs and internally work with Entity model
 */
@Service("FeedbackService")
public class FeedbackService {

    private final FeedbackRepository feedbacks;
    private final CategoryRepository categories;

    @Autowired
    public FeedbackService(FeedbackRepository feedbacks, CategoryRepository categories) {
        this.feedbacks = feedbacks;
        this.categories = categories;
    }

    /**
     * Method to get all feedback entities stored in DB
     * @return list of feedbacks converted into DTOs
     */
    public List<FeedbackDto> getAll() {
        return feedbacks.findAll().stream().map(FeedbackDto::new).collect(Collectors.toList());
    }

    /**
     * Method to get feedback entity with provided id, if there is one in DB.
     * Throws <c>FeedbackNotFoundException</c> in case entity with provided id was not found
     * Throws <c>InvalidIdentifierException</c> in case was provided invalid (not of UUID format) id
     * @param id String representation of id (UUID) of category
     * @return found feedback DTO
     */
    public FeedbackDto getOne(String id) {
        try {
            return feedbacks.findById(UUID.fromString(id))
                    .map(FeedbackDto::new)
                    .orElseThrow(() -> new FeedbackNotFoundException(id));
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }
    }

    /**
     * Saves input data (provided in form of FeedbackDto) into Feedback table and returns DTO of saved category
     * @param feedback data in form of DTO
     * @return FeedbackDto of saved category
     */
    public FeedbackDto save(FeedbackDto feedback) {
        Feedback toSave = new Feedback(feedback.getName(), feedback.getEmail(), feedback.getText());

        List<Category> categories = getCategoriesFromCategoryIds(feedback.getCategoryIds());
        toSave.setCategories(categories);

        return new FeedbackDto(feedbacks.save(toSave));
    }

    /**
     * Finds feedback from DB with provided id and sets its values to values provided as FeedbackDto argument.
     * Throws <c>InvalidIdentifierException</c> in case was provided invalid (not of UUID format) id
     * @param feedback CategoryDto with update data
     * @param id reference to category in DB which will be updated
     * @return CategoryDto of updated category
     */
    public FeedbackDto update(FeedbackDto feedback, String id) {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }

        Feedback updatedFeedback = feedbacks.findById(uuid)
                .map(fb -> {
                    fb.setName(feedback.getName());
                    fb.setEmail(feedback.getEmail());
                    fb.setText(feedback.getText());
                    fb.setCategories(getCategoriesFromCategoryIds(feedback.getCategoryIds()));
                    return feedbacks.save(fb);
                })
                .orElseGet(() -> {
                    Feedback newFeedback = getFeedbackFromFeedbackDto(feedback);
                    newFeedback.setId(uuid);
                    return feedbacks.save(newFeedback);
                });

        return new FeedbackDto(updatedFeedback);
    }

    /**
     * Delete category with PK given as argument
     * Throws <c>InvalidIdentifierException</c> in case was provided invalid (not of UUID format) id
     * @param id category PK
     */
    public void delete(String id) {
        try {
            feedbacks.deleteById(UUID.fromString(id));
        } catch (IllegalArgumentException iae) {
            throw new InvalidIdentifierException(id);
        }
    }

    private List<Category> getCategoriesFromCategoryIds(List<String> categoryIds) {
        if (categoryIds == null || categoryIds.isEmpty()) return new ArrayList<>();

        List<UUID> ids = categoryIds.stream().map(UUID::fromString).collect(Collectors.toList());
        return categories.findAllById(ids);
    }

    private Feedback getFeedbackFromFeedbackDto(FeedbackDto feedbackFromRequest) {

        Feedback newFeedback = new Feedback(feedbackFromRequest.getName(),
                feedbackFromRequest.getEmail(),
                feedbackFromRequest.getText());

        Date postTime = feedbackFromRequest.getTime() == null ? new Date() : feedbackFromRequest.getTime();
        newFeedback.setTime(postTime);

        newFeedback.setCategories(getCategoriesFromCategoryIds(feedbackFromRequest.getCategoryIds()));

        return newFeedback;
    }


}
