package ee.fujitsu.internshiptask.services.exceptions;


public class CategoryNotFoundException extends EntityNotFoundException {

    private static final String entityName = "Category";

    public CategoryNotFoundException(String id) {
        super(entityName, id);
    }
}
