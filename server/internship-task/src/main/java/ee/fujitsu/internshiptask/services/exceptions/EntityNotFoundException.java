package ee.fujitsu.internshiptask.services.exceptions;

/**
 * Derivative of this class should be thrown in case searched entity was not found by repository
 */
abstract class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String entityName, String id) {
        super(String.format("Not Found: %s with id %s", entityName, id));
    }

}
