package ee.fujitsu.internshiptask.services.dto;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * DTO for clients (will be exposed in controllers)
 */
@Data
@NoArgsConstructor
public class FeedbackDto {

    private String id;
    private String name;
    private String email;
    private String text;
    private Date time;
    private List<String> categoryIds;

    /**
     * Constructor to use instead of using mapper
     * @param feedback Feedback from DB that will be mapped into DTO
     */
    public FeedbackDto(Feedback feedback) {
        if (feedback == null) return;

        this.id = feedback.getId() != null ? feedback.getId().toString() : null;
        this.name = feedback.getName();
        this.email = feedback.getEmail();
        this.text = feedback.getText();
        this.time = feedback.getTime();
        this.categoryIds = getCategoryIds(feedback.getCategories());
    }

    private List<String> getCategoryIds(List<Category> categories) {
        if (categories == null || categories.isEmpty()) return new ArrayList<>();

        return categories.stream()
                .map(category -> {
                    try {
                        return category.getId().toString();
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
