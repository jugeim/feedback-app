package ee.fujitsu.internshiptask.repositories;

import ee.fujitsu.internshiptask.model.Category;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class CategoryRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CategoryRepository repository;

    @Test
    public void test_findById_should_return_category_when_find_by_existing_id() {
        Category testCategory = new Category("test category");
        entityManager.persistAndFlush(testCategory);

        Category fromDb = repository.findById(testCategory.getId()).orElse(null);
        assertThat(fromDb.getName()).isEqualTo(testCategory.getName());
    }

    @Test
    public void test_findById_should_return_null_when_find_by_not_existing_id() {
        UUID notExistingId = UUID.randomUUID();

        Category fromDb = repository.findById(notExistingId).orElse(null);
        assertThat(fromDb).isNull();
    }

    @Test
    public void test_findAll_should_return_all() {
        Category cat1 = new Category("category 1");
        Category cat2 = new Category("category 2");
        Category cat3 = new Category("category 3");

        entityManager.persist(cat1);
        entityManager.persist(cat2);
        entityManager.persist(cat3);
        entityManager.flush();

        List<Category> allCategories = repository.findAll();

        assertThat(allCategories)
                .hasSize(3)
                .extracting(Category::getName)
                .containsOnly(cat1.getName(), cat2.getName(), cat3.getName());
    }

    @Test
    public void test_save_should_save_category_without_catalog_category_reference() {
        Category category = new Category();
        String catName = "test_category";
        category.setName(catName);

        Category savedCategory = repository.save(category);

        assertThat(savedCategory)
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", catName);

        assertNotNull(savedCategory.getId(), "Category saved in DB should have id");
        assertNull(savedCategory.getCatalogCategory());
    }

    @Test
    public void test_save_should_save_category_with_catalog_category_reference() {
        Category catalogCategory = new Category();
        String catalogCategoryName = "test_category";
        catalogCategory.setName(catalogCategoryName);

        Category subcategory = new Category();
        String subcategoryName = "test_subcategory";
        subcategory.setName(subcategoryName);
        subcategory.setCatalogCategory(catalogCategory);

        Category savedCatalogCategory = repository.save(catalogCategory);
        Category savedSubCategory = repository.save(subcategory);

        assertThat(savedSubCategory)
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", subcategoryName);

        assertNotNull(savedSubCategory.getId(), "Category saved in DB should have id");
        assertNotNull(savedSubCategory.getCatalogCategory(), "Subcategory should reference the catalog category");

        assertEquals(savedSubCategory.getCatalogCategory(), savedCatalogCategory, String.format(
                "Subcategory should have reference to catalog category [%s] but was [%s]",
                savedSubCategory.getCatalogCategory().getName(),
                savedCatalogCategory.getName()
        ));
    }

    @Test
    public void test_delete() {
        Category testCategory = new Category("test category");
        entityManager.persistAndFlush(testCategory);

        UUID id = testCategory.getId();

        repository.deleteById(id);

        assertNull(repository.findById(id).orElse(null), "Entity should not be present");
    }

}