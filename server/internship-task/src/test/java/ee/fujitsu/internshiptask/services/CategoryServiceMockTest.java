package ee.fujitsu.internshiptask.services;


import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.services.dto.CategoryDto;
import ee.fujitsu.internshiptask.services.exceptions.CategoryNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class CategoryServiceMockTest {

    @MockBean
    private CategoryRepository mockRepository;

    private CategoryService service;

    @BeforeEach
    void setup() {
        this.service = new CategoryService(mockRepository);
    }


    @Test
    void test_getAll() {
        String cat1Name = "test 1";
        String cat2Name = "test 2";
        Category cat1 = new Category(cat1Name);
        Category cat2 = new Category(cat2Name);
        when(mockRepository.findAll()).thenReturn(List.of(cat1, cat2));

        List<CategoryDto> fromService = service.getAll();
        assertThat(fromService)
                .isNotNull()
                .hasSize(2)
                .extracting(CategoryDto::getName)
                .containsOnly(cat1Name, cat2Name);
    }

    @Test
    void test_getOne_should_find_if_existing() {
        UUID id = UUID.randomUUID();
        String catName = "test category with id " + id;
        Category cat = new Category(catName);
        when(mockRepository.findById(id)).thenReturn(Optional.of(cat));

        CategoryDto fromService = service.getOne(id.toString());
        assertThat(fromService)
                .isNotNull()
                .extracting(CategoryDto::getName)
                .isEqualTo(catName);
    }

    @Test
    void test_getOne_throws_exception_if_missing() {
        UUID id = UUID.randomUUID();
        when(mockRepository.findById(id)).thenReturn(Optional.empty());

        Exception exception = assertThrows(CategoryNotFoundException.class, () -> service.getOne(id.toString()));

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(id.toString()), "Should contain provided id " + id);
    }

    @Test
    void test_getOne_throws_exception_if_invalid_string_of_id_provided() {
        String invalidIdString = "ABC-bbc-bad-id";

        Exception exception = assertThrows(InvalidIdentifierException.class, () -> service.getOne(invalidIdString));

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(invalidIdString), "Should contain provided argument " + invalidIdString);
    }

    @Test
    void test_save_without_catalog_category() {
        UUID id = UUID.randomUUID();
        String categoryName = "testCategory";
        Category repoCategory = new Category(categoryName);
        repoCategory.setId(id);
        when(mockRepository.save(any(Category.class))).thenReturn(repoCategory);

        CategoryDto category = new CategoryDto();
        category.setName(categoryName);

        var savedCategory = service.save(category);

        assertNotNull(savedCategory);
        assertEquals(savedCategory.getId(), id.toString(),
                String.format("Should have id %s but was %s", id, savedCategory.getId()));
        assertTrue(savedCategory.getName().contains(categoryName));
    }

    @Test
    void test_save_with_catalog_category_id() {
        UUID catalogCategoryId = UUID.randomUUID();
        Category repoCatalogCategory = new Category();
        repoCatalogCategory.setId(catalogCategoryId);

        UUID subCategoryId = UUID.randomUUID();
        String subCategoryName = "test subcategory";
        Category repoSubCategory = new Category(subCategoryName);
        repoSubCategory.setId(subCategoryId);
        repoSubCategory.setCatalogCategory(repoCatalogCategory);

        when(mockRepository.save(any(Category.class))).thenReturn(repoSubCategory);

        CategoryDto category = new CategoryDto();
        category.setName(subCategoryName);
        category.setCatalogCategoryId(catalogCategoryId.toString());

        var savedCategory = service.save(category);

        assertNotNull(savedCategory);
        assertEquals(savedCategory.getCatalogCategoryId(), catalogCategoryId.toString(),
                String.format("Id should be %s but was %s", catalogCategoryId, savedCategory.getCatalogCategoryId()));
    }

    @Test
    void test_update_existing_category() {
        UUID id = UUID.randomUUID();
        String oldName = "old name";
        Category oldCategory = new Category(oldName);

        when(mockRepository.findById(id)).thenReturn(Optional.of(oldCategory));

        String newName = "new name";
        Category afterSave = new Category(newName);
        afterSave.setId(id);
        when(mockRepository.save(Mockito.any(Category.class))).thenReturn(afterSave);

        CategoryDto update = new CategoryDto();
        update.setName(newName);

        CategoryDto response = service.update(update, id.toString());

        assertNotNull(response);
        assertEquals(id.toString(), response.getId(),
                String.format("Id should not change (should be %s, but was %s)", id, response.getId()));
        assertEquals(newName, response.getName(),
                String.format("Name should be \"%s\", but was \"%s\"", newName, response.getName()));
    }

    @Test
    void test_update_not_existing_category() {
        UUID id = UUID.randomUUID();

        when(mockRepository.findById(id)).thenReturn(Optional.empty());

        String newName = "new name";
        Category afterSave = new Category(newName);
        afterSave.setId(id);
        when(mockRepository.save(Mockito.any(Category.class))).thenReturn(afterSave);

        CategoryDto update = new CategoryDto();
        update.setName(newName);

        CategoryDto response = service.update(update, id.toString());

        assertNotNull(response);
        assertEquals(id.toString(), response.getId(),
                String.format("Id should not change (should be %s, but was %s)", id, response.getId()));
        assertEquals(newName, response.getName(),
                String.format("Name should be \"%s\", but was \"%s\"", newName, response.getName()));
    }

    private Category createRepoCategory(String categoryName, UUID catalogCategoryId) {
        Category repoCatalogCategory = new Category();
        repoCatalogCategory.setId(catalogCategoryId);

        UUID subCategoryId = UUID.randomUUID();
        Category repoSubCategory = new Category(categoryName);
        repoSubCategory.setId(subCategoryId);
        repoSubCategory.setCatalogCategory(repoCatalogCategory);

        return repoSubCategory;
    }
}