package ee.fujitsu.internshiptask.web;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.repositories.FeedbackRepository;
import ee.fujitsu.internshiptask.services.CategoryService;
import ee.fujitsu.internshiptask.services.dto.CategoryDto;
import ee.fujitsu.internshiptask.services.dto.FeedbackDto;
import org.aspectj.util.Reflection;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import java.util.List;
import java.util.UUID;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FeedbackControllerTest {

    public static final ParameterizedTypeReference<List<FeedbackDto>> LIST_OF_FEEDBACKS
            = new ParameterizedTypeReference<>() {
    };
    public static final String FB1_NAME = "Tester RESTer";
    public static final String FB2_EMAIL = "test@email.org";
    public static final String FB3_TEXT = "All good!";
    public static final String CAT1_NAME = "first category";
    public static final String CAT2_NAME = "second category";

    private static final String BASE_URL = "/api/feedbacks";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @BeforeEach
    private void loadTestData() {
        Feedback fb1 = new Feedback(FB1_NAME, "email", "text");
        Feedback fb2 = new Feedback("name", FB2_EMAIL, "text");
        Feedback fb3 = new Feedback("name", "email", FB3_TEXT);

        Category cat1 = new Category(CAT1_NAME);
        Category cat2 = new Category(CAT2_NAME);

        fb1.setCategories(List.of(cat1, cat2));
        fb2.setCategories(List.of(cat2));

        categoryRepository.saveAll(List.of(cat1, cat2));

        feedbackRepository.saveAll(List.of(fb1, fb2, fb3));
    }

    @AfterEach
    private void clearData() {
        feedbackRepository.deleteAll();
        categoryRepository.deleteAll();
    }


    @Test
    void test_all_returns_list_of_feedbacks() {
        ResponseEntity<List<FeedbackDto>> entity = restTemplate.exchange(BASE_URL, HttpMethod.GET, null, LIST_OF_FEEDBACKS);
        assertNotNull(entity);
        List<FeedbackDto> feedbacks = entity.getBody();

        assertTrue(isNotEmpty(feedbacks));
        assertTrue(feedbacks.stream().anyMatch(h -> h.getName().equals(FB1_NAME)));
        assertTrue(feedbacks.stream().anyMatch(h -> h.getEmail().equals(FB2_EMAIL)));
        assertTrue(feedbacks.stream().anyMatch(h -> h.getText().equals(FB3_TEXT)));

    }

    @Test
    void should_save_feedback_without_categories() {
        String fbName = "Mart Tram";
        FeedbackDto fbToSave = new FeedbackDto();
        fbToSave.setName(fbName);

        HttpEntity<FeedbackDto> httpEntity = new HttpEntity<>(fbToSave);
        ResponseEntity<FeedbackDto> entity = restTemplate.exchange(BASE_URL, HttpMethod.POST, httpEntity, FeedbackDto.class);
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());

        FeedbackDto feedback = entity.getBody();
        assertNotNull(feedback);
        assertNotNull(feedback.getId());
        assertEquals(fbName, feedback.getName());
        assertTrue(entity.getHeaders().getLocation().toString().contains(feedback.getId()),
                String.format("Should be reference to created category in server response Location header (was %s)",
                        entity.getHeaders().getLocation()));

    }

    @Test
    void should_save_feedback_with_categories() {
        UUID catIdFromDb = getCategoryIdFromDb(CAT1_NAME);
        FeedbackDto fbToSave = new FeedbackDto();
        fbToSave.setName("Mart Tram");
        fbToSave.setCategoryIds(List.of(catIdFromDb.toString()));

        HttpEntity<FeedbackDto> httpEntity = new HttpEntity<>(fbToSave);
        ResponseEntity<FeedbackDto> entity = restTemplate.exchange(BASE_URL, HttpMethod.POST, httpEntity, FeedbackDto.class);
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());

        FeedbackDto feedback = entity.getBody();
        assertNotNull(feedback);
        assertNotNull(feedback.getId());
        assertEquals(catIdFromDb.toString(), feedback.getCategoryIds().get(0));

        // ensure saved in db also
        Feedback inDb = feedbackRepository.findById(UUID.fromString(feedback.getId())).orElseThrow();
        Category catInDb = categoryRepository.findById(catIdFromDb).orElseThrow();
        assertEquals(catInDb, inDb.getCategories().get(0));
    }

    @Test
    void find_feedback_by_id() {
        var fbFromDb = feedbackRepository.findAll().stream().findAny().orElseThrow();
        String requestUrl = String.format("%s/%s", BASE_URL, fbFromDb.getId());

        ResponseEntity<FeedbackDto> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, FeedbackDto.class);
        assertNotNull(entity);
        FeedbackDto feedback = entity.getBody();
        assertNotNull(feedback);
        assertEquals(fbFromDb.getName(), feedback.getName());
        assertEquals(fbFromDb.getEmail(), feedback.getEmail());
        assertEquals(fbFromDb.getText(), feedback.getText());
    }

    @Test
    void find_category_by_id_unknown_id() {
        var id = UUID.randomUUID().toString();
        String requestUrl = String.format("%s/%s", BASE_URL, id);

        assertThrows(RestClientException.class,
                () -> restTemplate.exchange(requestUrl, HttpMethod.GET, null, FeedbackDto.class));

        ResponseEntity<String> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, String.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test
    void find_category_by_id_invalid_id() {
        String invalidId = "push-pop-drop-top";
        String requestUrl = String.format("%s/%s", BASE_URL, invalidId);

        assertThrows(RestClientException.class,
                () -> restTemplate.exchange(requestUrl, HttpMethod.GET, null, FeedbackDto.class));

        ResponseEntity<String> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void test_replaceFeedback() {
        var feedbackFromDb = feedbackRepository.findAll()
                .stream()
                .filter(fb -> FB2_EMAIL.equals(fb.getEmail()))
                .findFirst().orElseThrow();
        Hibernate.initialize(feedbackFromDb.getCategories());
        var oldCategory = feedbackFromDb.getCategories().get(0);

        String newName = "New FB Name";
        String newEmail = "New FB Email";
        String newText = "New FB Text";
        var newCategoryId = getCategoryIdFromDb(CAT1_NAME);
        assertNotEquals(oldCategory.getId(), newCategoryId, "Something went wrong, categories should be different");

        FeedbackDto updateFb = new FeedbackDto();
        updateFb.setName(newName);
        updateFb.setEmail(newEmail);
        updateFb.setText(newText);
        updateFb.setCategoryIds(List.of(newCategoryId.toString()));

        String requestUrl = String.format("%s/%s", BASE_URL, feedbackFromDb.getId());
        HttpEntity<FeedbackDto> httpEntity = new HttpEntity<>(updateFb);
        ResponseEntity<FeedbackDto> entity = restTemplate.exchange(requestUrl, HttpMethod.PUT, httpEntity, FeedbackDto.class);

        assertNotNull(entity);
        FeedbackDto feedback = entity.getBody();
        assertNotNull(feedback);
        assertEquals(newName, feedback.getName());
        assertEquals(newEmail, feedback.getEmail());
        assertEquals(newText, feedback.getText());
        assertEquals(newCategoryId.toString(), feedback.getCategoryIds().get(0));
    }

    @Test
    void test_deleteFeedback() {
        Feedback toDelete = new Feedback("will be deleted soon", "liame", "qweqweqwe");
        var fbFromDb = feedbackRepository.save(toDelete);

        String requestUrl = String.format("%s/%s", BASE_URL, fbFromDb.getId());
        ResponseEntity<FeedbackDto> entity = restTemplate.exchange(requestUrl, HttpMethod.DELETE, null, FeedbackDto.class);

        assertEquals(HttpStatus.NO_CONTENT, entity.getStatusCode());
        assertFalse(categoryRepository.findById(fbFromDb.getId()).isPresent());

    }

    private UUID getCategoryIdFromDb(String categoryName) {

        return categoryRepository.findAll().stream().filter(c -> categoryName.equals(c.getName()))
            .map(Category::getId).findFirst().orElseThrow();
    }

}