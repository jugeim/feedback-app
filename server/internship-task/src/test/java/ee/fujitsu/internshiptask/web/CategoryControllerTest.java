package ee.fujitsu.internshiptask.web;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.services.dto.CategoryDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import java.util.List;
import java.util.UUID;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CategoryControllerTest {

    public static final ParameterizedTypeReference<List<CategoryDto>> LIST_OF_CATEGORIES
            = new ParameterizedTypeReference<>() {
    };
    public static final String CAT_PATIENT_PORTAL = "Patients portal";
    public static final String CAT_DOCTOR_PORTAL = "Doctors portal";
    public static final String CAT_HEALTH = "Health";

    private static final String BASE_URL = "/api/categories";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CategoryRepository categoryRepository;

    @BeforeEach
    private void loadTestData() {
        Category c1 = new Category(CAT_HEALTH);
        Category c2 = new Category(CAT_DOCTOR_PORTAL);
        Category c3 = new Category(CAT_PATIENT_PORTAL);

        categoryRepository.saveAll(List.of(c1, c2, c3));

    }

    @AfterEach
    private void clearData() {
        categoryRepository.deleteAll();
    }

    @Test
    void test_all_returns_list_of_categories() {
        ResponseEntity<List<CategoryDto>> entity = restTemplate.exchange(BASE_URL, HttpMethod.GET, null, LIST_OF_CATEGORIES);
        assertNotNull(entity);
        List<CategoryDto> categories = entity.getBody();

        assertTrue(isNotEmpty(categories));
        assertTrue(categories.stream().anyMatch(h -> h.getName().equals(CAT_HEALTH)));
        assertTrue(categories.stream().anyMatch(h -> h.getName().equals(CAT_DOCTOR_PORTAL)));
        assertTrue(categories.stream().anyMatch(h -> h.getName().equals(CAT_PATIENT_PORTAL)));

    }

    @Test
    void should_save_category() {
        String categoryName = "TEST CATEGORY";
        CategoryDto categoryToSave = new CategoryDto();
        categoryToSave.setName(categoryName);

        HttpEntity<CategoryDto> httpEntity = new HttpEntity<>(categoryToSave);
        ResponseEntity<CategoryDto> entity = restTemplate.exchange(BASE_URL, HttpMethod.POST, httpEntity, CategoryDto.class);
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());


        CategoryDto category = entity.getBody();
        assertNotNull(category);
        assertNotNull(category.getId());
        assertEquals(categoryName, category.getName());
        assertTrue(entity.getHeaders().getLocation().toString().contains(category.getId()),
                String.format("Should be reference to created category in server response Location header (was %s)",
                        entity.getHeaders().getLocation()));

    }

    @Test
    void find_category_by_id() {
        var categoryFromDb = categoryRepository.findAll().stream().findAny().orElseThrow();
        String requestUrl = String.format("%s/%s", BASE_URL, categoryFromDb.getId());

        ResponseEntity<CategoryDto> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, CategoryDto.class);
        assertNotNull(entity);
        CategoryDto category = entity.getBody();
        assertNotNull(category);
        assertEquals(categoryFromDb.getName(), category.getName());
    }

    @Test
    void find_category_by_id_unknown_id() {
        var id = UUID.randomUUID().toString();
        String requestUrl = String.format("%s/%s", BASE_URL, id);

        assertThrows(RestClientException.class,
                () -> restTemplate.exchange(requestUrl, HttpMethod.GET, null, CategoryDto.class));

        ResponseEntity<String> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, String.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test
    void find_category_by_id_invalid_id() {
        String invalidId = "push-pop-drop-top";
        String requestUrl = String.format("%s/%s", BASE_URL, invalidId);

        assertThrows(RestClientException.class,
                () -> restTemplate.exchange(requestUrl, HttpMethod.GET, null, CategoryDto.class));

        ResponseEntity<String> entity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void test_replaceCategory() {
        var oldCatalog = categoryRepository.findAll().stream().findAny().orElseThrow();
        Category toDb = new Category("Name Before", oldCatalog);
        var categoryFromDb = categoryRepository.save(toDb);

        String newName = "Name After";
        var newCatalog = categoryRepository.findAll().stream().findAny().orElseThrow();
        CategoryDto updateCategory = new CategoryDto();
        updateCategory.setName(newName);
        updateCategory.setCatalogCategoryId(newCatalog.getId().toString());

        String requestUrl = String.format("%s/%s", BASE_URL, categoryFromDb.getId());
        HttpEntity<CategoryDto> httpEntity = new HttpEntity<>(updateCategory);
        ResponseEntity<CategoryDto> entity = restTemplate.exchange(requestUrl, HttpMethod.PUT, httpEntity, CategoryDto.class);

        assertNotNull(entity);
        CategoryDto category = entity.getBody();
        assertNotNull(category);
        assertEquals(newName, category.getName());
        assertEquals(newCatalog.getId().toString(), category.getCatalogCategoryId());
    }

    @Test
    void test_deleteCategory() {
        Category toDelete = new Category("will be deleted soon");
        var categoryFromDb = categoryRepository.save(toDelete);

        String requestUrl = String.format("%s/%s", BASE_URL, categoryFromDb.getId());
        ResponseEntity<CategoryDto> entity = restTemplate.exchange(requestUrl, HttpMethod.DELETE, null, CategoryDto.class);

        assertEquals(HttpStatus.NO_CONTENT, entity.getStatusCode());
        assertFalse(categoryRepository.findById(categoryFromDb.getId()).isPresent());

    }
}