package ee.fujitsu.internshiptask.services;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import ee.fujitsu.internshiptask.repositories.CategoryRepository;
import ee.fujitsu.internshiptask.repositories.FeedbackRepository;
import ee.fujitsu.internshiptask.services.dto.FeedbackDto;
import ee.fujitsu.internshiptask.services.exceptions.FeedbackNotFoundException;
import ee.fujitsu.internshiptask.services.exceptions.InvalidIdentifierException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class FeedbackServiceMockTest {

    @MockBean
    private FeedbackRepository mockRepository;

    @MockBean
    private CategoryRepository mockCategoryRepository;

    private FeedbackService service;

    @BeforeEach
    void setup() {
        this.service = new FeedbackService(mockRepository, mockCategoryRepository);
    }


    @Test
    void test_getAll() {
        String feedback1Name = "test 1";
        Feedback feedback1 = new Feedback(feedback1Name, "feedback1Email", "feedback1Text");

        String feedback2Name = "test 2";
        Feedback feedback2 = new Feedback(feedback2Name, "feedback2Email", "feedback2Text");
        when(mockRepository.findAll()).thenReturn(List.of(feedback1, feedback2));

        List<FeedbackDto> fromService = service.getAll();
        assertThat(fromService)
                .isNotNull()
                .hasSize(2)
                .extracting(FeedbackDto::getName)
                .containsOnly(feedback1Name, feedback2Name);
    }

    @Test
    void test_getOne_should_find_if_existing() {
        UUID id = UUID.randomUUID();
        String feedbackName = "feedback with id " + id;
        String feedbackEmail = "feedback email";
        String feedbackText = "feedback text";
        Date feedbackTime = new Date();
        Feedback feedback = new Feedback(feedbackName, feedbackEmail, feedbackText);
        feedback.setTime(feedbackTime);

        when(mockRepository.findById(id)).thenReturn(Optional.of(feedback));

        FeedbackDto fromService = service.getOne(id.toString());

        assertNotNull(fromService, "Should return feedback but returned null");
        assertEquals(feedbackName, fromService.getName(),
                String.format("Name in response should be %s but was %s", feedbackName, fromService.getName()));
        assertEquals(feedbackEmail, fromService.getEmail(),
                String.format("Email in response should be %s but was %s", feedbackEmail, fromService.getEmail()));
        assertEquals(feedbackText, fromService.getText(),
                String.format("Text in response should be %s but was %s", feedbackText, fromService.getText()));
        assertEquals(feedbackTime, fromService.getTime(),
                String.format("Time in response should be %s but was %s", feedbackTime, fromService.getTime()));
    }

    @Test
    void test_getOne_throws_exception_if_missing() {
        UUID id = UUID.randomUUID();
        when(mockRepository.findById(id)).thenReturn(Optional.empty());

        Exception exception = assertThrows(FeedbackNotFoundException.class, () -> service.getOne(id.toString()));

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(id.toString()), "Should contain provided id " + id);
    }

    @Test
    void test_getOne_throws_exception_if_invalid_string_of_id_provided() {
        String invalidIdString = "ABC-bbc-bad-id";

        Exception exception = assertThrows(InvalidIdentifierException.class, () -> service.getOne(invalidIdString));

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(invalidIdString), "Should contain provided argument " + invalidIdString);
    }

    @Test
    void test_save_without_categories() {
        UUID id = UUID.randomUUID();
        String feedbackName = "test name";
        String feedbackEmail = "test email";
        String feedbackText = "test text";
        Feedback repoFeedback = new Feedback(feedbackName, feedbackEmail, feedbackText);
        repoFeedback.setId(id);
        when(mockRepository.save(any(Feedback.class))).thenReturn(repoFeedback);

        FeedbackDto feedbackToSave = new FeedbackDto();
        feedbackToSave.setName(feedbackName);
        feedbackToSave.setEmail(feedbackEmail);
        feedbackToSave.setText(feedbackText);

        var savedFeedback = service.save(feedbackToSave);

        assertNotNull(savedFeedback);
        assertEquals(savedFeedback.getId(), id.toString(),
                String.format("Should have id %s but was %s", id, savedFeedback.getId()));
        assertEquals(feedbackName, savedFeedback.getName(),
                String.format("Name in response should be %s but was %s", feedbackName, savedFeedback.getName()));
        assertEquals(feedbackEmail, savedFeedback.getEmail(),
                String.format("Email in response should be %s but was %s", feedbackEmail, savedFeedback.getEmail()));
        assertEquals(feedbackText, savedFeedback.getText(),
                String.format("Text in response should be %s but was %s", feedbackText, savedFeedback.getText()));
    }

    @Test
    void test_save_with_categories() {
        UUID cat1Id = UUID.randomUUID();
        Category cat1 = new Category("cat1");
        cat1.setId(cat1Id);
        UUID cat2Id = UUID.randomUUID();
        Category cat2 = new Category("cat2");
        cat2.setId(cat2Id);

        when(mockCategoryRepository.findAllById(List.of(cat1Id, cat2Id))).thenReturn(List.of(cat1, cat2));
        when(mockCategoryRepository.findById(cat2Id)).thenReturn(Optional.of(cat2));

        UUID id = UUID.randomUUID();
        Feedback repoFeedback = new Feedback("feedbackName", "feedbackEmail", "feedbackText");
        repoFeedback.setId(id);
        repoFeedback.setCategories(List.of(cat1, cat2));

        when(mockRepository.save(any(Feedback.class))).thenReturn(repoFeedback);

        FeedbackDto feedbackToSave = new FeedbackDto();
        feedbackToSave.setName("feedbackName");
        feedbackToSave.setEmail("feedbackEmail");
        feedbackToSave.setText("feedbackText");
        feedbackToSave.setCategoryIds(List.of(cat1Id.toString(), cat2Id.toString()));

        var savedFeedback = service.save(feedbackToSave);

        assertNotNull(savedFeedback);
        assertEquals(savedFeedback.getId(), id.toString(),
                String.format("Should have id %s but was %s", id, savedFeedback.getId()));
        assertThat(savedFeedback.getCategoryIds())
                .isNotNull()
                .hasSize(2)
                .containsOnly(cat1Id.toString(), cat2Id.toString());
    }

    @Test
    void test_save_with_invalid_category_id() {
        UUID categoryId = UUID.randomUUID();

        Feedback feedback = new Feedback();

        when(mockCategoryRepository.findById(categoryId)).thenReturn(Optional.empty());
        when(mockRepository.save(any(Feedback.class))).thenReturn(feedback);

        FeedbackDto Feedback = new FeedbackDto();
        Feedback.setCategoryIds(List.of(categoryId.toString()));

        var savedFeedback = service.save(Feedback);

        assertNotNull(savedFeedback);
        assertTrue(savedFeedback.getCategoryIds().isEmpty(),"Invalid references to categories should not be saved");
    }

    @Test
    void test_update_existing_feedback() {
        UUID id = UUID.randomUUID();
        String oldName = "old name";
        String oldEmail = "old email";
        String oldText = "old text";
        Feedback oldFeedback = new Feedback(oldName, oldEmail, oldText);
        when(mockRepository.findById(id)).thenReturn(Optional.of(oldFeedback));

        String newName = "new name";
        String newEmail = "new email";
        String newText = "new text";
        Feedback afterSave = new Feedback(newName, newEmail, newText);
        afterSave.setId(id);
        when(mockRepository.save(Mockito.any(Feedback.class))).thenReturn(afterSave);

        FeedbackDto updateFb = new FeedbackDto();
        updateFb.setName(newName);
        updateFb.setEmail(newEmail);
        updateFb.setText(newText);

        FeedbackDto response = service.update(updateFb, id.toString());

        assertNotNull(response);
        assertEquals(id.toString(), response.getId(),
                String.format("Id should not change (should be %s, but was %s)", id, response.getId()));
        assertEquals(newName, response.getName(),
                String.format("Name should be \"%s\", but was \"%s\"", newName, response.getName()));
        assertEquals(newEmail, response.getEmail(),
                String.format("Email should be \"%s\", but was \"%s\"", newEmail, response.getEmail()));
        assertEquals(newText, response.getText(),
                String.format("Text should be \"%s\", but was \"%s\"", newText, response.getText()));
    }

    @Test
    void test_update_not_existing_feedback() {
        UUID id = UUID.randomUUID();
        when(mockRepository.findById(id)).thenReturn(Optional.empty());

        String newName = "new name";
        String newEmail = "new email";
        String newText = "new text";
        Feedback afterSave = new Feedback(newName, newEmail, newText);
        afterSave.setId(id);
        when(mockRepository.save(Mockito.any(Feedback.class))).thenReturn(afterSave);

        FeedbackDto updateFb = new FeedbackDto();
        updateFb.setName(newName);
        updateFb.setEmail(newEmail);
        updateFb.setText(newText);

        FeedbackDto response = service.update(updateFb, id.toString());

        assertNotNull(response);
        assertEquals(id.toString(), response.getId(),
                String.format("Id should not change (should be %s, but was %s)", id, response.getId()));
        assertEquals(newName, response.getName(),
                String.format("Name should be \"%s\", but was \"%s\"", newName, response.getName()));
        assertEquals(newEmail, response.getEmail(),
                String.format("Email should be \"%s\", but was \"%s\"", newEmail, response.getEmail()));
        assertEquals(newText, response.getText(),
                String.format("Text should be \"%s\", but was \"%s\"", newText, response.getText()));
    }
}