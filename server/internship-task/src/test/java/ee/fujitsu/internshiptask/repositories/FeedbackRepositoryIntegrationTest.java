package ee.fujitsu.internshiptask.repositories;

import ee.fujitsu.internshiptask.model.Category;
import ee.fujitsu.internshiptask.model.Feedback;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class FeedbackRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FeedbackRepository repository;

    @Test
    public void test_findById_should_return_Feedback_when_find_by_existing_id() {
        String name = "test name";
        String email = "test email";
        String text = "test text";
        Feedback testFeedback = new Feedback(name, email, text);
        entityManager.persistAndFlush(testFeedback);

        Feedback fromDb = repository.findById(testFeedback.getId()).orElse(null);

        assertNotNull(fromDb, "Existing feedback was not found by its id");

        assertEquals(name, fromDb.getName(),
                String.format("Feedback name should be %s but was %s", name, fromDb.getName()));
        assertEquals(email, fromDb.getEmail(),
                String.format("Feedback email should be %s but was %s", email, fromDb.getEmail()));
        assertEquals(text, fromDb.getText(),
                String.format("Feedback text should be %s but was %s", text, fromDb.getText()));
    }

    @Test
    public void test_findById_should_return_null_when_find_by_not_existing_id() {
        UUID notExistingId = UUID.randomUUID();

        Feedback fromDb = repository.findById(notExistingId).orElse(null);
        assertThat(fromDb).isNull();
    }

    @Test
    public void test_findAll_should_return_all() {
        Feedback feedback1 = new Feedback("n1", "em1", "txt1");
        Feedback feedback2 = new Feedback("n2", "em2", "txt2");
        Feedback feedback3 = new Feedback("n3", "em3", "txt3");

        entityManager.persist(feedback1);
        entityManager.persist(feedback2);
        entityManager.persist(feedback3);
        entityManager.flush();

        List<Feedback> allFeedbacks = repository.findAll();

        assertThat(allFeedbacks)
                .hasSize(3)
                .extracting(Feedback::getName)
                .containsOnly(feedback1.getName(), feedback2.getName(), feedback3.getName());
    }

    @Test
    public void test_save_should_save_Feedback_without_categories() {
        Feedback Feedback = new Feedback();
        String feedbackName = "test_Feedback";
        Feedback.setName(feedbackName);

        Feedback savedFeedback = repository.save(Feedback);

        assertThat(savedFeedback).isNotNull();
        assertNotNull(savedFeedback.getId(), "Feedback saved in DB should have id");
        assertThat(savedFeedback.getCategories()).isEmpty();
    }

    @Test
    public void test_save_should_save_Feedback_with_categories() {
        Category cat1 = new Category("category 1");
        Category cat2 = new Category("category 2");

        entityManager.persist(cat1);
        entityManager.persist(cat2);
        entityManager.flush();

        Feedback feedback = new Feedback();
        String feedbackName = "test_subFeedback";
        feedback.setName(feedbackName);
        feedback.setCategories(List.of(cat1, cat2));

        Feedback savedFeedback = repository.save(feedback);

        assertThat(savedFeedback).isNotNull();
        assertNotNull(savedFeedback.getId(), "Feedback saved in DB should have id");
        assertThat(savedFeedback.getCategories())
                .isNotNull()
                .hasSize(2)
                .extracting(Category::getName)
                .containsOnly(cat1.getName(), cat2.getName());
    }

    @Test
    public void test_deleteById() {
        Feedback testFeedback = new Feedback("name", "email", "text");
        entityManager.persistAndFlush(testFeedback);

        UUID id = testFeedback.getId();

        repository.deleteById(id);

        assertNull(repository.findById(id).orElse(null), "Entity should not be present");
    }

}