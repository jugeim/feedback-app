## Fujitsu internship test task

Author: Juri Geiman

### Server:

Default location: //localhost:8080  
Feedbacks on: /api/feedbacks/  
Categories on: /api/categories/  

##### Commands (from `...\feedback-app\server\internship-task>`):

- Run server:
```
mvn spring-boot:run
```
- Run tests:
```
mvn test
```

### Client:

Default location: //localhost:9000  
Feedbacks view on: /feedbacks   

##### Commands (from `...\feedback-app\client>`): 
- Before run:
```
npm install
```
- Run client:
```
npm start
```
- Run tests in terminal:
```
npx cypress run
```
- Run tests with GUI:
```
npx cypress open
```

