import { log } from 'node:console';
/// <reference types="cypress" />

import { should } from "chai";

beforeEach(() => {
    cy.visit('');
});

describe('Test categories SelectList', () => { 
    it('should display simple category structure', () => {
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');
        cy.visit('/feedbacks');
        cy.wait('@loadCategories');

        cy.get('#inputCategory>option').as('selectListOptions');

        cy.get('@selectListOptions').should('have.length', 3);
        cy.get("@selectListOptions").first().should('have.attr', 'disabled');
        cy.get("@selectListOptions").eq(1).should('to.contain', '- ').and('not.have.attr', 'disabled');
        cy.get("@selectListOptions").eq(2).should('to.contain', '- ').and('not.have.attr', 'disabled');


    });

    it('should display complex category structure', () => {
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-complex-struct.json' }).as('loadCategories');
        cy.visit('/feedbacks');
        cy.wait('@loadCategories');

        cy.get('#inputCategory>option').as('selectListOptions');

        cy.get('@selectListOptions').should('have.length', 12);

        cy.get("@selectListOptions").get("option:contains(Catalog)").each(($el) => expect($el).to.have.attr("disabled"));
        cy.get("@selectListOptions").get("option:contains(Selectable)").each(($el) => expect($el).to.not.have.attr("disabled"));
    });
});

describe('Test sending feedbacks', () => {
    it('should not post, if form state is invaild', () => {
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');
        cy.visit('/feedbacks');
        cy.wait('@loadCategories');

        cy.get('#inputName').should("have.attr", "type", "text").and('have.attr', 'required');
        cy.get('#inputEmail').should("have.attr", "type", "email").and('have.attr', 'required');
        cy.get('#inputFeedback').should('have.attr', 'required');
    });

    it('can send feedbacks', () => {
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');
        cy.intercept('POST', '*api/feedbacks*', (req) => {
            req.redirect('#', 201) // do not send data to the server;

            let contentType = req.headers['content-type'].toString().toLowerCase();

            console.log(contentType.length);

            expect(req.body).to.haveOwnProperty('name', 'name')
            expect(req.body).to.haveOwnProperty('email', 'e@mail.com')
            expect(req.body).to.haveOwnProperty('text', 'cy text')
            expect(req.body).to.haveOwnProperty('categoryIds').to.contain('2');
        })

        cy.visit('/feedbacks');
        cy.wait('@loadCategories');

        cy.get('#inputName').type('name')
        cy.get('#inputEmail').type('e@mail.com')
        cy.get('#inputFeedback').type('cy text')
        cy.get('#inputCategory').first().select("2"); // value == '2'
        
        cy.get('button.w-100').as('submitBtn');

        cy.get("@submitBtn").click();
    })
}); 