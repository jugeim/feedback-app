/// <reference types="cypress" />

beforeEach(() => {
    cy.visit('');
});


describe('One Feedback in table', () => {
    it('should show all feedback data', () => {
        cy.intercept('GET', '*api/feedbacks*', { fixture: 'test-feedbacks-one.json' }).as('loadFeedbacks');
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');

        cy.visit('/feedbacks');

        cy.wait('@loadFeedbacks');
        cy.wait('@loadCategories');

        cy.get('table>tbody').find('tr').as("fbRow")
            
        cy.get('@fbRow')
            .should('have.length', 1)
            .find('.col-0')
            .should('have.text', '1');
        
        cy.get('@fbRow')
            .find('.col-1').should('contain.text', '3');

        cy.get('@fbRow')
            .find('.col-2').eq(0).should('contain.text', 'Cypress Tester');

        cy.get('@fbRow')
            .find('.col-2').eq(1).should('contain.text', 'test@cypress.cy');

        cy.get('@fbRow')
            .find('.col-3').should('contain.text', 'Doctors portal').and('contain.text', 'Patients portal');

        cy.get('@fbRow')
            .find('.col-4').should('contain.text', 'Test text');
    });

    it('should hide data of too long feedback', () => {
        cy.intercept('GET', '*api/feedbacks*', { fixture: 'test-feedbacks-one-complex.json' }).as('loadFeedbacks');
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');

        cy.visit('/feedbacks');

        cy.wait('@loadFeedbacks');
        cy.wait('@loadCategories');

        cy.get('table>tbody').find('tr').should('have.length', 1);
        cy.get('table>tbody').find('tr').should('have.attr', 'data-view', "collapsed");
    });

    it('feedback row should be expandable', () => {
        cy.intercept('GET', '*api/feedbacks*', { fixture: 'test-feedbacks-one-complex.json' }).as('loadFeedbacks');
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');

        cy.visit('/feedbacks');

        cy.wait('@loadFeedbacks');
        cy.wait('@loadCategories');

        cy.get('table>tbody').find('tr').click().then(() => {
            cy.get('table>tbody').find('tr').first().should('have.attr', 'data-view', "expanded");
        });
    });
});

describe('Many Feedbacks in table', () => {
    it('should render all feedbacks', () => {
        cy.intercept('GET', '*api/feedbacks*', { fixture: 'test-feedbacks-many.json' }).as('loadFeedbacks');
        cy.intercept('GET', '*api/categories*', { fixture: 'test-categories-simple-struct.json' }).as('loadCategories');

        cy.visit('/feedbacks');

        cy.wait('@loadFeedbacks');
        cy.wait('@loadCategories');

        cy.get('table>tbody>tr').as('rows');
        cy.get('@rows').should('have.length', 16);

        cy.get('@rows').each((row, $index) => {
            let firstCol = row.find('.col-0').get()[0];
            expect(firstCol).to.have.text(`${$index + 1}`);

            row.trigger('click');
            expect(firstCol).to.have.text('>');
        });
    });
});