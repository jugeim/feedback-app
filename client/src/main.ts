import Aurelia, { RouterConfiguration } from 'aurelia';
import { MyApp } from './my-app';

import "bootstrap"
import 'bootstrap/dist/css/bootstrap.min.css'

import '../static/css/signin.css'
import '../static/css/site.css'

Aurelia
.register(RouterConfiguration)
  .app(MyApp)
  .start();
