import { HttpClient, inject, json } from 'aurelia';
import { IFeedback } from './../domain/IFeedback';
import { IDao } from './IDao';

const URL = "//localhost:8080/api/feedbacks";

@inject(HttpClient)
export class FeedbackDao implements IDao<IFeedback> {

    constructor(private httpClient: HttpClient) {

    }
    
    async getAll(): Promise<IFeedback[]> {
        const response = await this.httpClient
            .get(URL, { cache: "no-store" });

        if (response.ok) {
            const data = (await response.json());
            return data as IFeedback[];
        }
        return [];
    }
    
    async getOne(id: string): Promise<IFeedback> {
        const response = await this.httpClient
            .get(`${URL}/${id}`, { cache: "no-store" });

        if (response.ok) {
            const data = (await response.json());

            return data as IFeedback;
        }
        throw new Error('Error in server response? Response: ' + response.status);

    }

    async postOne(item: IFeedback): Promise<IFeedback> {
        const response = await this.httpClient.fetch(URL, { method: "post", 
                                                            headers: { "Content-Type": "application/json"} , 
                                                            body: json(item)}
                                                    );

        const data = await response.json();

        return data as IFeedback;
    }

}