import { ICategory } from './../domain/ICategory';
import { HttpClient } from "@aurelia/fetch-client";
import { inject } from "@aurelia/kernel";
import { IDao } from "./IDao";

const URL = "//localhost:8080/api/categories"

@inject(HttpClient)
export class CategoryDao implements IDao<ICategory> {

    constructor(private httpClient: HttpClient) {

    }
    
    async getAll(): Promise<ICategory[]> {
        const response = await this.httpClient
            .get(URL, { cache: "no-store" });

        if (response.ok) {
            const data = (await response.json());
            return data as ICategory[];
        }
        return [];
    }
    
    async getOne(id: string): Promise<ICategory> {
        const response = await this.httpClient
            .get(`${URL}/${id}`, { cache: "no-store" });

        console.log(response);

        if (response.ok) { 
            const data = (await response.json());
            return data as ICategory;
        }
        throw new Error('Error in server response? Response: ' + response.status);
    }
    
    async postOne(item: ICategory): Promise<void> {
        throw new Error(`Method not implemented. Argument passed: ${item}`);
    }

}