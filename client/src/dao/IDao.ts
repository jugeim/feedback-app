/**
 * Inteface should be implemented by classes that directly communicate with server.
 */
export interface IDao<T> {
  
    getAll() : Promise<T[]>;
    getOne(id: string) : Promise<T>;
    postOne(item: T): Promise<T | void>;
  }