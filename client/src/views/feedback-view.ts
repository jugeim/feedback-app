import { EventAggregator, IDisposable } from "aurelia";
import { IFeedback } from "../domain/IFeedback";
import { FeedbackService } from "../services/feedback-service";
import { CategoryVm } from "../view-models/category-vm";
import { FeedbackVm } from "../view-models/feedback-vm";

export class FeedbackView {

    feedbacks: FeedbackVm[];
    sortedCategories: CategoryVm[];

    private subscriptions: IDisposable[] = [];

    constructor(
        private eventAggregator: EventAggregator,
        private feedbackService: FeedbackService) {

        this.subscriptions.push(
        this.eventAggregator.subscribe('post-feedback', (newFeedback: IFeedback) => this.postFeedback(newFeedback))
        );

    }

    async attached(): Promise<void> {
        await this.getData();
    }

    async postFeedback(newFeedback: IFeedback): Promise<void> {
        await this.feedbackService.postFeedback(newFeedback)
    }

    private async getData(): Promise<void> {
        this.sortedCategories = await this.getCategories();
        this.feedbacks = await this.getFeedbacks();
    }
    
    private async getFeedbacks() : Promise<FeedbackVm[]> {

        return await this.feedbackService.getFeedbacks();
    }

    private async getCategories() : Promise<CategoryVm[]> {

        return await this.feedbackService.getSortedCategories();
    }
}