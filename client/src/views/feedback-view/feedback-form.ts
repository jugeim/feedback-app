import { CategoryVm } from './../../view-models/category-vm';
import { EventAggregator } from '@aurelia/kernel';
import { bindable } from '@aurelia/runtime-html';


export class FeedbackForm {

    @bindable categories: CategoryVm[] = [];
    name : string;
    email: string;
    text: string;
    selectedCategories: string[] = [];

    constructor(private eventAggregator: EventAggregator) {
    }

    postFeedback(): void {
        const newFeedback = { name: this.name, email: this.email, categoryIds: this.selectedCategories, text: this.text, time: new Date() };
        this.eventAggregator.publish('post-feedback', newFeedback);
    }

}