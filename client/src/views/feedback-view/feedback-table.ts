import { bindable } from "aurelia";
import { FeedbackVm } from "../../view-models/feedback-vm";

export class FeedbackTable {

    @bindable feedbacks: FeedbackVm[] = [];

    toggleFeedback(rowIndex: number): void {
        const elem = document.querySelector(`tbody>:nth-child(${rowIndex + 1})`) as HTMLElement;

        const rowView = elem.dataset.view;
        if (rowView === "collapsed") {
            this.expand(elem);
        } else {
            this.collapse(elem, rowIndex);
        }

    }

    expand(row: HTMLElement): void {
        this.collapseAll();

        row.dataset.view = "expanded";
        for (const child of row.childNodes) {
            if ('scope' in child && child['scope'] ==='row') (child as HTMLElement).innerText = ">";
            (child as HTMLElement)?.classList?.add("expanded");
        }
    }

    collapse(row: HTMLElement, index: number): void {
        row.dataset.view = "collapsed";
        for (const child of row.childNodes) {
            if ('scope' in child && child['scope'] ==='row') (child as HTMLElement).innerText = index.toString();
            (child as HTMLElement)?.classList?.remove("expanded");
        }
    }

    collapseAll(): void {
        const rows = document.getElementsByTagName('tbody').item(0).children;

        let i = 0;
        for (const row of rows) {
            if (row.tagName != "TR") continue;

            this.collapse(row as HTMLElement, ++i);
        }
    }

}