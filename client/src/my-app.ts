import { FeedbackService } from './services/feedback-service';
import { EventAggregator, IDisposable, inject, route } from "aurelia";

@route({
  routes: [
    { id: 'au', path: '', component: import('./views/title-view'), title: 'title' },
    { path: 'feedbacks', component: import('./views/feedback-view'), title: 'feedbacks' },
  ]
})
@inject(FeedbackService)
export class MyApp {
  
  private subscriptions: IDisposable[] = [];

  constructor(private eventAggregator: EventAggregator,
              private feedbackService: FeedbackService) {
    
  }


  detached() : void {
    this.subscriptions.forEach(subscription => subscription.dispose());
    this.subscriptions = [];
  }

}
