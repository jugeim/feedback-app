import { IFeedback } from './../domain/IFeedback';
import { FeedbackVm } from './../view-models/feedback-vm';
import { CategoryVm } from './../view-models/category-vm';
import { FeedbackDao } from './../dao/feedback-dao';
import { CategoryDao } from './../dao/category-dao';
import { inject } from "aurelia";

@inject(CategoryDao, 
        FeedbackDao)
export class FeedbackService {

    private categories?: CategoryVm[] // as the client does not have methods to affect categories in server, they can be loaded once per session

    constructor(private categoryDao: CategoryDao, 
                private feedbackDao: FeedbackDao) {

    }

    async getFeedbacks(): Promise<FeedbackVm[]> {
        const feedbacksFromService = this.feedbackDao.getAll();

        const categories = await this.getCategories();

        return (await feedbacksFromService).map(f => new FeedbackVm(f, categories))
    }

    async getCategories(): Promise<CategoryVm[]> {
        if (this.categories) return [...this.categories];
        
        return [...await this.loadCategories()];
    }

    // maybe the sorting is not service job (should the service provide categories int the way they should be "showed"?)
    async getSortedCategories(): Promise<CategoryVm[]> {
        const unsorted = await this.getCategories();
        const result: CategoryVm[] = unsorted.filter(cat => !cat.catalogCategoryId); // take the topmost categories right away

        let count = 0;
        let maxCount = unsorted.length - result.length + 1;
        while(unsorted.length > 0) {
            count++;
            if(count > maxCount) {
                console.log("propbably stuck into endless loop while sorting categories");
                break;
            }

            const category = unsorted.shift();

            // should be already in result array and already has the minimal level (0)
            if (!category.catalogCategoryId) continue;

            const index = result.findIndex(e => e.id == category.catalogCategoryId);
            if (index < 0) {
                unsorted.push(category); // if needed catalog category is currently missing, then return to the queue 
                continue;
            }  

            const catalogCategory = result[index];
            catalogCategory.disabled = true;
            category.level = catalogCategory.level + 1;

            // inser category on the caorrect position
            if (index + 1 > result.length) result.push(category);
            else result.splice(index + 1, 0, category);

            // reset counter and decrease cycle limit
            count = 0
            maxCount--;
        }
        return result;
    }

    async postFeedback(feedback: IFeedback): Promise<void> {

        await this.feedbackDao.postOne(feedback);
    }

    private async loadCategories(): Promise<CategoryVm[]> {
        const categoriesFromServer =  await this.categoryDao.getAll();

        const res = categoriesFromServer.map(c => new CategoryVm(c));
        this.categories = res;
        return res;
    }
}