export interface ICategory {
    id?: string;
    name: string;
    catalogCategoryId: string | null;
}