export interface IFeedback {
    id?: string;
    name: string;
    email: string;
    text: string;
    time: Date;
    categoryIds: string[];
}