import { ICategory } from './../domain/ICategory';
import { IFeedback } from './../domain/IFeedback';
export class FeedbackVm {
    id?: string;
    name: string;
    email: string;
    text: string;
    time: string;
    categories: string[];

    constructor(feedback:IFeedback, categories: ICategory[]) {
        this.id = feedback.id;
        this.name = feedback.name;
        this.email = feedback.email;
        this.text = feedback.text;
        this.time = new Date(feedback.time).toLocaleString('default', { month: 'short', day: 'numeric' });
        this.categories = feedback.categoryIds
            .map(catId => categories.find(cat => cat.id === catId)?.name);
    }
}