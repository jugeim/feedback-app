import { ICategory } from "../domain/ICategory";

export class CategoryVm {
    id: string;
    name: string;
    level: number
    disabled: boolean;
    catalogCategoryId: string;

    constructor(category: ICategory) {
        this.id = category.id;
        this.name = category.name;
        this.catalogCategoryId = category.catalogCategoryId;
        this.level = 0;
        this.disabled = false;
    }
}